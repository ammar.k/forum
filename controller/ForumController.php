<?php

    namespace Controller;

    use App\Session;
    use App\AbstractController;
    use App\ControllerInterface;
    use Model\Managers\TopicManager;
    use Model\Managers\PostManager;
    use Model\Managers\CategoryManager;
    use Model\Managers\VisitorManager;
    
    class ForumController extends AbstractController implements ControllerInterface{

        public function index(){
            
        }

        public function listAllTopics(){
            $topicManager = new TopicManager();
            return [
                "view" => VIEW_DIR."forum/listTopics.php",
                "data" => [
                    "topics" => $topicManager->findAll(["creationDate", "DESC"])
                ]
            ];
        }

        public function listCategories(){
            $categoryManager = new CategoryManager();
            return [
                "view" => VIEW_DIR."forum/listCategory.php",
                "data" => [
                    "categories" => $categoryManager->allCategory()
                ]
            ];
        
        }

        public function listTopics($id){
            $topicManager = new TopicManager();
            $categoryManager = new CategoryManager();
            

            return [
                "view" => VIEW_DIR."forum/listTopics.php",
                "data" => [
                    "topics" => $topicManager->findByCategory($id),
                    "category" => $categoryManager->findOneById($id)
                ]
            ];
        }

        public function listPosts($id){
            $postManager = new PostManager();
            $topicmanager = new TopicManager();
            $categoryManager = new CategoryManager();
            $topic = $topicmanager->findOneById($id);
            $id_cat = $topic->getCategory()->getId();
            
            return [
                "view" => VIEW_DIR."forum/listPosts.php",
                "data" => [
                    "posts" => $postManager->findByTopic($id),
                    "topic" => $topicmanager->findOneById($id),
                    "category" => $categoryManager->findOneById($id_cat)
                ]
            ];
        }

        public function addCategory(){
            if(Session::getUser() == null){
                Session::addFlash("error", "you have to be connected to add a category");
                return $this->redirectTo("security", "loginPage");
            }
            else{
                $user = Session::getUser();
            
            $categoryManager = new CategoryManager();
            if(isset($_POST["submit"]) && !empty($_POST["name"])){
                $name = $_POST["name"];
                $categoryManager->add(["name" => $name]);
                //header("Location: index.php?ctrl=forum&action=listCategories");
            }
            return $this->redirectTo("forum", "listCategories");
        }
        }

        public function addTopic($id){
            //echo "test";die;
            if(Session::getUser() == null){
                Session::addFlash("error", "you have to be connected to add a topic");
                return $this->redirectTo("security", "loginPage");
            }
            else{
                $user = Session::getUser();
                $topicManager = new TopicManager();
                $postManager = new PostManager();
                if(isset($_POST["submit"]) && !empty($_POST["title"]) && !empty($_POST["content"])){
                    $title = $_POST["title"];
                    $content = $_POST["content"];
                    $topic_id = $topicManager->add(["title" => $title, "visitor_id" => $user->getId() , "category_id" => $id, "locked" => 0]);
                    //$topic = $topicManager->findOneByTitle($title);
                    $postManager->add(["content" => $content, "visitor_id" => $user->getId() , "topic_id" => $topic_id, "creationDate" => (new \DateTime())->format("Y-m-d H:i:s")]);
                }
            }
//            return $this->listTopics($id);
            return $this->redirectTo("forum", "listTopics", $id);
        }

        public function addPost($id){
            if(Session::getUser() == null){
                Session::addFlash("error", "you have to be connected to add a post");
                return $this->redirectTo("security", "loginPage");
            }
            else{
                $user = Session::getUser();
            $postManager = new PostManager();
            if(isset($_POST["submit"]) && !empty($_POST["content"])){
                $content = $_POST["content"];
                $postManager->add(["content" => $content, "visitor_id" => $user->getId() , "topic_id" => $id, "creationDate" => (new \DateTime())->format("Y-m-d H:i:s")]);
                
            }
        }
            //return $this->listPosts($id);
            return $this->redirectTo("forum", "listPosts", $id);
        }

        public function deleteCategory($id){
            if(Session::getUser() == null){
                Session::addFlash("error", "you have to be connected to delete a category");
                return $this->redirectTo("security", "loginPage");
            }
            else{
                $user = Session::getUser();
            
            $categoryManager = new CategoryManager();
            $categoryManager->delete($id);
            //return $this->listCategories();
            return $this->redirectTo("forum", "listCategories");
            }
        }

        public function deleteTopic($id){
            if(Session::getUser() == null){
                Session::addFlash("error", "you have to be connected to delete a topic");
                return $this->redirectTo("security", "loginPage");
            }
            else{
                $user = Session::getUser();

                $topicManager = new TopicManager();
                
                $topicToDelete = $topicManager->findOneById($id);
                $idCat = $topicToDelete->getCategory()->getId();
                if($user->getId() == $topicToDelete->getVisitor()->getId()){

                    $topicManager->delete($id);
                    Session::addFlash("success", "topic deleted");
            } else {
                Session::addFlash("error", "you don't have the right to delete this topic");
            }
        }

                return $this->redirectTo("forum", "listTopics", $idCat);
        }

        public function deletePost($id){
            if(Session::getUser() == null){
                Session::addFlash("error", "you have to be connected to delete a post");
                return $this->redirectTo("security", "loginPage");
            }
            else{
                $user = Session::getUser();
                $postManager = new PostManager();
                $postToDelete = $postManager->findOneById($id);
                $idTopic = $postToDelete->getTopic()->getId();
                if($user->getId() == $postToDelete->getVisitor()->getId()){
                    $postManager->delete($id);
                    Session::addFlash("success", "Post deleted");
                } else {
                    Session::addFlash("error", "you don't have the right to delete this post");
                }
            }
            return $this->redirectTo("forum", "listPosts", $idTopic);
        }
 
        public function modifyCategory($id){
            if(Session::getUser() == null){
                Session::addFlash("error", "you have to be connected to modify a category");
                return $this->redirectTo("security", "loginPage");
            }
            else{
                $categoryManager = new CategoryManager();
                if(isset($_POST["button1"]))
                {
                    //limte the delete to the admin
                    $name = $_POST["name"];
                    $categoryManager->update($id,$name);
                }
        }
            return $this->redirectTo("forum", "listCategories");
        }
    
        public function modifyTopic($id){
            if(Session::getUser() == null){
                Session::addFlash("error", "you have to be connected to modify a topic");
                return $this->redirectTo("security", "loginPage");
            }
            else{
                $user = Session::getUser();
                $topicManager = new TopicManager();
                $topicToModify = $topicManager->findOneById($id);
                $idCat = $topicToModify->getCategory()->getId();
                if(isset($_POST["button1"]))
                {
                    if($user->getId() == $topicToModify->getVisitor()->getId()){
                    $title = $_POST["title"];
                    $topicManager->update($id,$title);
                    Session::addFlash("success", "Topic modified");
                }
                else{
                    Session::addFlash("error", "you don't have the right to modify this topic");
                }
                }
            }
            return $this->redirectTo("forum", "listTopics", $idCat);
        }

        public function modifyPost($id){
            if(Session::getUser() == null){
                Session::addFlash("error", "you have to be connected to modify a post");
                return $this->redirectTo("security", "loginPage");
            }
            else{
                $user = Session::getUser();
                $postManager = new PostManager();
                $postToModify = $postManager->findOneById($id);
                $idTopic = $postToModify->getTopic()->getId();
                if(isset($_POST["button1"])
                ){
                    if($user->getId() == $postToModify->getVisitor()->getId()){
                    $content = $_POST["content"];
                    $postManager->update($id,$content);
                    Session::addFlash("success", "Post modified");
                }
                else{
                    Session::addFlash("error", "you don't have the right to modify this post");
                }
                }
            }
            return $this->redirectTo("forum", "listPosts", $idTopic);
        }



        public function locktopic($id){
            if(Session::getUser() == null){
                Session::addFlash("error", "you have to be connected to lock a topic");
                return $this->redirectTo("security", "loginPage");
            }
            else{
                $user = Session::getUser();
                $topicManager = new TopicManager();
                $topicToLock = $topicManager->findOneById($id);
                $idCat = $topicToLock->getCategory()->getId();
                if($user->getId() == $topicToLock->getVisitor()->getId()){
                    $topicManager->lock($id);
                    Session::addFlash("success", "Topic locked");
                }
                else{
                    Session::addFlash("error", "you don't have the right to lock this topic");
                }
            }
            return $this->redirectTo("forum", "listTopics", $idCat);
        }

        public function unlocktopic($id){
            if(Session::getUser() == null){
                Session::addFlash("error", "you have to be connected to unlock a topic");
                return $this->redirectTo("security", "loginPage");
            }
            else{
                $user = Session::getUser();
                $topicManager = new TopicManager();
                $topicToUnlock = $topicManager->findOneById($id);
                $idCat = $topicToUnlock->getCategory()->getId();
                if($user->getId() == $topicToUnlock->getVisitor()->getId()){
                    $topicManager->unlock($id);
                    Session::addFlash("success", "Topic locked");
                }
                else{
                    Session::addFlash("error", "you don't have the right to unlock this topic");
                }
            }
            return $this->redirectTo("forum", "listTopics", $idCat);
        }

        
        public function infoUser($id){
            $topicManager = new TopicManager();
            $visitorManager = new VisitorManager();
            $postManager = new PostManager();

            return [
                "view" => VIEW_DIR."visitors/profile.php",
                "data" => [
                    "topics" => $topicManager->findByUser($id),
                    "user" => $visitorManager->findAllByUser($id),
                    "posts" => $postManager->findByUser($id)
                ]
            ];
        }

        public function changeName(){
            
            $visitorManager = new VisitorManager();
            $user = Session::getUser();
            $id = $user->getId();
            if(isset($_POST["button1"]) && !empty($_POST["username"]))
            {

                $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                if($username){
                    if($visitorManager->findUsername($username) != null){
                        Session::addFlash("error", "Username already taken");
                    }
                    else{
                    $visitorManager->updateName($id,$username);
                    Session::addFlash("success", "Username modified");
                }
            }
        }
            return $this->redirectTo("forum", "infoUser", $id);
        }

        public function changeMail(){
            $visitorManager = new VisitorManager();
            $user = Session::getUser();
            $id = $user->getId();
            if(isset($_POST["button1"]) && !empty($_POST["mail"]))
            {
                $mail = filter_input(INPUT_POST, 'mail', FILTER_SANITIZE_FULL_SPECIAL_CHARS,FILTER_VALIDATE_EMAIL);
                if($mail){
                    if($visitorManager->findMail($mail) != null){
                        Session::addFlash("error", "Email already taken");
                    }
                    else{
                        $visitorManager->updateMail($id,$mail);
                        Session::addFlash("success", "Email modified");
                    }
                }
                
            }
            return $this->redirectTo("forum", "infoUser", $id);
        }

        public function changePass(){
            $visitorManager = new VisitorManager();
            $user = Session::getUser();
            $id = $user->getId();
            if(isset($_POST["button1"]) && !empty($_POST["oldPass"]) && !empty($_POST["pass1"]) && !empty($_POST["pass2"]))
            {
                $oldPass = filter_input(INPUT_POST, 'oldPass', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                $pass1 = filter_input(INPUT_POST, 'pass1', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                $pass2 = filter_input(INPUT_POST, 'pass2', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                if($oldPass && $pass1 && $pass2)
                {
                    if($pass1 == $pass2)
                    {
                        if(password_verify($oldPass, $user->getPassWord()))
                        {
                            $pass1 = password_hash($pass1, PASSWORD_DEFAULT);
                            $visitorManager->updatePass($id,$pass1);
                            Session::addFlash("success", "Password modified");
                        }
                        else{
                            Session::addFlash("error", "Wrong password");
                        }
                    }
                }
            }
            return $this->redirectTo("forum", "infoUser", $id);

        }

        public function changePhoto(){
            //specifies the directory where the file is going to be placed
            $target_dir = "public/img/";
            //specifies the path of the file to be uploaded
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $uploadOk = 1;
            //holds the file extension of the file
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
            if(isset($_POST["button1"])) {
                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                if($check !== false) {
                    Session::addFlash("success", "File is an image - " . $check["mime"] . ".");
                    $uploadOk = 1;
                } else {
                    Session::addFlash("error", "File is not an image.");
                    $uploadOk = 0;
                }
            }
            // Check if file already exists
            if (file_exists($target_file)) {
                Session::addFlash("error", "Sorry, file already exists.");
                $uploadOk = 0;
            }
            // Check file size
            if ($_FILES["fileToUpload"]["size"] > 5000000) {
                Session::addFlash("error", "Sorry, your file is too large.");
                $uploadOk = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "jpeg") {
                Session::addFlash("error", "Sorry, only JPG and JPEG files are allowed.");
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                Session::addFlash("error", "Sorry, your file was not uploaded.");
            }
            // if everything is ok, try to upload file
            else {
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    Session::addFlash("success", "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.");
                } else {
                    Session::addFlash("error", "Sorry, there was an error uploading your file.");
                }
            }
            $visitorManager = new VisitorManager();
            $user = Session::getUser();
            $id = $user->getId();
            $title = $_FILES["fileToUpload"]["name"];
            //var_dump($title);die;
            //check if the there where no file to uplaod
            if($title == ""){
                Session::addFlash("error", "No file to upload");
            }
            else{
                $visitorManager->updatePhoto($id,$title);
                Session::addFlash("success", "Photo modified");
            }
            return $this->redirectTo("forum", "infoUser", $id);
        }

}



        

    


