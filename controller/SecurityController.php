<?php

namespace Controller;

use App\Session;
use App\AbstractController;
use App\ControllerInterface;
use Model\Managers\TopicManager;
use Model\Managers\PostManager;
use Model\Managers\CategoryManager;
use Model\Managers\VisitorManager;

class SecurityController extends AbstractController implements ControllerInterface {

    public function index() {
        return [
            "view" => VIEW_DIR . "home.php",
        ];
    }

    public function registerPage(){
        return [
            "view" => VIEW_DIR . "security/register.php",
        ];
    }
    public function loginPage(){
        return [
            "view" => VIEW_DIR . "security/login.php",
        ];
    }

    public function register(){

        $visitorManager = new VisitorManager();
        if(isset($_POST['username']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password2'])
        && !empty($_POST['username']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['password2'])){

        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_FULL_SPECIAL_CHARS,FILTER_VALIDATE_EMAIL);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $password2 = filter_input(INPUT_POST, 'password2', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        if($username && $email && $password && $password2){
            $mail = $visitorManager->findMail($email);
            $user = $visitorManager->findUsername($username);
            if(!$user && !$mail){
                if($password == $password2 && strlen($password) >= 5){
                    $password = password_hash($password, PASSWORD_DEFAULT);
                    $visitorManager->add([
                        'username' => $username,
                        'eMail' => $email,
                        'passWord' => $password,
                        'inscriptionDate' => date('Y-m-d H:i:s')
                    ]);
                    Session::addFlash('success', 'Votre compte a bien été créé !');
                    $this->redirectTo("forum", "listCategories");

                }
                else{
                    Session::addFlash('danger', 'Les mots de passe ne correspondent pas ou sont trop courts !');
                    $this->redirectTo('security', 'registerPage');
                }
            }
                else if($user){
                    Session::addFlash('error', 'Ce nom d\'utilisateur est déjà pris');
                    $this->redirectTo('security', 'registerPage');
                }
                else{
                Session::addFlash('error', 'Cette adresse mail est déjà utilisée');
                $this->redirectTo('security', 'registerPage');
                }
              
        }
        }
    }

    public function login(){
        $visitorManager = new VisitorManager();

        if(isset($_POST['email']) && isset($_POST['passWord']) && !empty($_POST['email']) && !empty($_POST['passWord'])){

        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_FULL_SPECIAL_CHARS,FILTER_VALIDATE_EMAIL);
        $password = filter_input(INPUT_POST, 'passWord', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        
        if($email && $password){
            $user = $visitorManager->findMail($email);
            if($user){
                if(password_verify($password, $user->getPassWord())){
                    Session::setUser($user);
                    Session::addFlash('success', 'Vous êtes connecté !');
                    $this->redirectTo("forum", "listCategories");
                }
                else{
                    Session::addFlash('danger', 'Mot de passe incorrect !');
                    $this->redirectTo('security', 'loginPage');
                }
            }
            else{
                Session::addFlash('danger', 'Cet email n\'existe pas !');
                $this->redirectTo('security', 'loginPage');
            }
        }
        }
    }

    public function logout(){
        Session::removeUser();
        Session::addFlash('success', 'Vous êtes déconnecté !');
        $this->redirectTo("forum", "listCategories");
    }

    public function listUsers(){
        $visitorManager = new VisitorManager();
        $users = $visitorManager->findAll();
        return [
            "view" => VIEW_DIR . "visitors/users.php",
            "data" => [
                "users" => $users
            ]
        ];
    }
    public function makeAdmin($id){
        $visitorManager = new VisitorManager();
        $role = "ROLE_ADMIN";
        $visitorManager->updateRole($id,$role);
        Session::addFlash('success', 'The user is now an admin !');
        $this->redirectTo("security", "listUsers");
    }
    public function makeModerator($id){
        $visitorManager = new VisitorManager();
        $role = "ROLE_MODERATOR";
        $visitorManager->updateRole($id,$role);
        Session::addFlash('success', 'The user is now a moderator !');
        $this->redirectTo("security", "listUsers");
    }

    public function removeRole($id){
        $visitorManager = new VisitorManager();
        $role = "ROLE_USER";
        $visitorManager->updateRole($id,$role);
        Session::addFlash('success', 'The user is now a simple user !');
        $this->redirectTo("forum", "listCategories");
    }

    public function removeUser($id){
        $visitorManager = new VisitorManager();
        $visitorManager->deleteUser($id);
        Session::addFlash('success', 'The user has been deleted !');
        $this->redirectTo("security", "listUsers");

        
    }
    
}


