<?php
    namespace Model\Managers;
    
    use App\Manager;
    use App\DAO;
    use Model\Managers\PostManager;

    class PostManager extends Manager{

        protected $className = "Model\Entities\Post";
        protected $tableName = "Post";


        public function __construct(){
            parent::connect();
        }

        public function findByTopic($id){
            $sql = "SELECT *
            FROM ".$this->tableName." i
            WHERE i.topic_id = :id";

            return $this->getMultipleResults(
                DAO::select($sql, ['id'=>$id]), 
                $this->className
            );
        }

        //update content of a post
        public function update($id, $content){
            $sql = "UPDATE ".$this->tableName." SET content = :content WHERE id_post = :id";
            return DAO::update($sql, ['id'=>$id, 'content'=>$content]);
        }

        public function findByUser($id){
            $sql = "SELECT * FROM ".$this->tableName." WHERE visitor_id = :id";

            return $this->getMultipleResults(
                DAO::select($sql, ['id'=>$id]), 
                $this->className
            );
        }
        public function anonymUser($id){
            $sql = "UPDATE ".$this->tableName." SET visitor_id = NULL WHERE visitor_id = :id";
            return DAO::update($sql, ['id'=>$id]);
        }
    }


    

?>