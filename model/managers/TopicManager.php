<?php
    namespace Model\Managers;
    
    use App\Manager;
    use App\DAO;
    use Model\Managers\TopicManager;

    class TopicManager extends Manager{

        protected $className = "Model\Entities\Topic";
        protected $tableName = "Topic";


        public function __construct(){
            parent::connect();
        }

        //list all topics in a category
        public function findByCategory($id){
            $sql = "SELECT *,
            (SELECT COUNT(*) FROM Post WHERE Post.topic_id = Topic.id_topic) AS nbPosts,
            (SELECT MAX(Post.creationDate) FROM Post WHERE Post.topic_id = Topic.id_topic) AS lastPost
            FROM ".$this->tableName." WHERE category_id = :id";

            return $this->getMultipleResults(
                DAO::select($sql, ['id'=>$id]), 
                $this->className
            );
        }

        public function findByUser($id){
            $sql = "SELECT * FROM ".$this->tableName." WHERE visitor_id = :id";

            return $this->getMultipleResults(
                DAO::select($sql, ['id'=>$id]), 
                $this->className
            );
        }

        public function update($id, $title){
            $sql = "UPDATE ".$this->tableName." SET title = :title WHERE id_topic = :id";
            return DAO::update($sql, ['id'=>$id, 'title'=>$title]);
        }

        public function lock($id){
            $sql = "UPDATE ".$this->tableName." SET locked = 1 WHERE id_topic = :id";
            return DAO::update($sql, ['id'=>$id]);
        }

        public function unlock($id){
            $sql = "UPDATE ".$this->tableName." SET locked = 0 WHERE id_topic = :id";
            return DAO::update($sql, ['id'=>$id]);
        }

        public function anonymUser($id){
            $sql = "UPDATE ".$this->tableName." SET visitor_id = NULL WHERE visitor_id = :id";
            return DAO::update($sql, ['id'=>$id]);
        }
    }

?>