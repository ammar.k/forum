<?php
    namespace Model\Managers;
    
    use App\Manager;
    use App\DAO;
    use Model\Managers\CategoryManager;
    use Model\Managers\TopicManager;

    class CategoryManager extends Manager{

        protected $className = "Model\Entities\Category";
        protected $tableName = "Category";


        public function __construct(){
            parent::connect();
        }

        //count topics in a category
        public function allCategory(){
            $sql = "SELECT *,
            (SELECT COUNT(*) FROM Topic WHERE Topic.category_id = Category.id_category) AS nbTopics,
            (SELECT MAX(Topic.creationDate) FROM Topic WHERE Topic.category_id = Category.id_category) AS lastTopic
            FROM ".$this->tableName." ";

            return $this->getMultipleResults(
                DAO::select($sql), 
                $this->className
            );
        }

        public function update($id, $name){
            $sql = "UPDATE ".$this->tableName." SET name = :name WHERE id_category = :id";
            return DAO::update($sql, ['id'=>$id, 'name'=>$name]);
        }
        
    }

?>