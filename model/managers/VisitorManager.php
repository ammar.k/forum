<?php
    namespace Model\Managers;
    
    use App\Manager;
    use App\DAO;
    use Model\Managers\VisitorManager;

    class VisitorManager extends Manager{

        protected $className = "Model\Entities\Visitor";
        protected $tableName = "Visitor";


        public function __construct(){
            parent::connect();
        }

        public function findMail($email){
            $sql = "SELECT * FROM $this->tableName WHERE eMail = :email";

            return $this->getOneOrNullResult(
                DAO::select($sql, ['email' => $email], false), 
                $this->className
            );
        }

        public function findUsername($username){
            $sql = "SELECT * FROM $this->tableName WHERE username = :username";

            return $this->getOneOrNullResult(
                DAO::select($sql, ['username' => $username], false), 
                $this->className
            );
        }

        public function findAllByUser($id){
            $sql = "SELECT *,
            (SELECT COUNT(*) FROM Topic WHERE Topic.visitor_id = Visitor.id_visitor) AS nbTopics,
            (SELECT COUNT(*) FROM Post WHERE Post.visitor_id = Visitor.id_visitor) AS nbPosts
            FROM $this->tableName WHERE id_visitor = :id";

            return $this->getOneOrNullResult(
                DAO::select($sql, ['id' => $id], false), 
                $this->className
            );
        }

        public function infoAllUsers(){
            $sql = "SELECT *,
            (SELECT COUNT(*) FROM Topic WHERE Topic.visitor_id = Visitor.id_visitor) AS nbTopics,
            (SELECT COUNT(*) FROM Post WHERE Post.visitor_id = Visitor.id_visitor) AS nbPosts
            FROM $this->tableName";

            return $this->getMultipleResults(
                DAO::select($sql, [], false), 
                $this->className
            );
        }

        public function updateRole($id, $role){
            $sql = "UPDATE ".$this->tableName." SET role = :role WHERE id_visitor = :id";
            return DAO::update($sql, ['id'=>$id, 'role'=>$role]);
        }

        public function deleteUser($id){
        
            $topicManager = new TopicManager();
            $postManager = new PostManager();
            $topicManager->anonymUser($id);
            $postManager->anonymUser($id);
            $sql = "DELETE FROM ".$this->tableName." WHERE id_visitor = :id";
            return DAO::delete($sql, ['id' => $id]); 
        }

        public function updateName($id, $username){
            $sql = "UPDATE ".$this->tableName." SET username = :username WHERE id_visitor = :id";
            return DAO::update($sql, ['id'=>$id, 'username'=>$username]);
        }

        public function updateMail($id, $email){
            $sql = "UPDATE ".$this->tableName." SET eMail = :email WHERE id_visitor = :id";
            return DAO::update($sql, ['id'=>$id, 'email'=>$email]);
        }

        public function updatePass($id, $password){
            $sql = "UPDATE ".$this->tableName." SET password = :password WHERE id_visitor = :id";
            return DAO::update($sql, ['id'=>$id, 'password'=>$password]);
        }

        public function updatePhoto($id,$title){
            $sql = "UPDATE ".$this->tableName." SET photoTitle = :title WHERE id_visitor = :id";
            return DAO::update($sql, ['id'=>$id, 'title'=>$title]);
        }

    }





?>