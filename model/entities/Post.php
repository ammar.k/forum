<?php

namespace Model\Entities;
use App\Entity;

    final class Post extends Entity{
        private $id;
        private $content;
        private $creationDate;
        private $visitor;
        private $topic;

        public function __construct($data){         
            $this->hydrate($data);        
        }

        /**
         * Get the value of id
         */
        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */
        public function setId($id)
        {
                $this->id = $id;
                return $this;
        }

        /**
         * Get the value of content
         */
        public function getContent()
        {
                return $this->content;
        }

        /**
         * Set the value of content
         *
         * @return  self
         */
        public function setContent($content)
        {
                $this->content = $content;
                return $this;
        }

        public function getCreationDate(){
                //date today
                $date = new \DateTime();
                //check if the date is today
                if($date->format('d/m/Y') == $this->creationDate->format('d/m/Y')){
                    //if it is, return only the time
                    $formattedDate = $this->creationDate->format("H:i:s");
                }
                else{
                    //if not, return just the date
                    $formattedDate = $this->creationDate->format("d/m/Y");
                }
                return $formattedDate;
            }
    
            public function setCreationDate($date){
                $this->creationDate = new \DateTime($date);
                return $this;
            }

        /**
         * Get the value of user
         */
        public function getVisitor()
        {
                return $this->visitor;
        }

        /**
         * Set the value of user
         *
         * @return  self
         */

        public function setVisitor($visitor)
        {
                $this->visitor = $visitor;
                return $this;
        }

        /**
         * Get the value of topic
         */
        public function getTopic()
        {
                return $this->topic;
        }

        /**
         * Set the value of topic
         *
         * @return  self
         */
        public function setTopic($topic)
        {
                $this->topic = $topic;
                return $this;
        }

        public function getExcerpt(){
                $excerpt = substr($this->content, 0, 100);
                $excerpt .= '...';
                return $excerpt;
        }

        
    }

?>