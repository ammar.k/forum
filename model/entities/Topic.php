<?php
    namespace Model\Entities;

    use App\Entity;

    final class Topic extends Entity{

        private $id;
        private $title;
        private $creationDate;
        private $locked;
        private $visitor;
        private $category;
        private $nbPosts;
        private $lastPost;

        public function __construct($data){         
            $this->hydrate($data);        
        }
 
        /**
         * Get the value of id
         */ 
        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */ 
        public function setId($id)
        {
                $this->id = $id;

                return $this;
        }

        /**
         * Get the value of title
         */ 
        public function getTitle()
        {
                return $this->title;
        }

        /**
         * Set the value of title
         *
         * @return  self
         */ 
        public function setTitle($title)
        {
                $this->title = $title;

                return $this;
        }

        /**
         * Get the value of user
         */ 
        public function getVisitor()
        {
                return $this->visitor;
        }

        /**
         * Set the value of user
         *
         * @return  self
         */ 
        public function setVisitor($visitor)
        {
                $this->visitor = $visitor;

                return $this;
        }

        public function getCreationDate(){
            //date today
            $date = new \DateTime();
            //check if the date is today
            if($date->format('d/m/Y') == $this->creationDate->format('d/m/Y')){
                //if it is, return only the time
                $formattedDate = $this->creationDate->format("H:i:s");
            }
            else{
                //if not, return just the date
                $formattedDate = $this->creationDate->format("d/m/Y");
            }
            return $formattedDate;
        
        }

        public function setCreationDate($date){
            $this->creationDate = new \DateTime($date);
            return $this;
        }

        /**
         * Get the value of closed
         */ 
        public function getLocked()
        {
                return $this->locked;
        }

        /**
         * Set the value of closed
         *
         * @return  self
         */ 
        public function setLocked($locked)
        {
                $this->locked = $locked;

                return $this;
        }

        /**
         * Get the value of category
         */
        public function getCategory()
        {
                return $this->category;
        }

        /**
         * Set the value of category
         *
         * @return  self
         */
        public function setCategory($category)
        {
                $this->category = $category;

                return $this;
        }
        
        /**
         * Get the value of nbPosts
         */
        public function getNbPosts()
        {
                return $this->nbPosts;
        }
        
        /**
         * Set the value of nbPosts
         *
         * @return  self
         */
        public function setNbPosts($nbPosts)
        {
                $this->nbPosts = $nbPosts;
                
                return $this;
        }

        /**
         * Get the value of lastPost
         */

        public function getLastPost()
        {
                return $this->lastPost;
        }
        
        /**
         * Set the value of lastPost
         *
         * @return  self
         */

        public function setLastPost($lastPost)
        {
                $this->lastPost = $lastPost;
                
                return $this;
        }
        

        public function __toString()
        {
                return $this->title;
        }
    }
