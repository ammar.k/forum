<?php

namespace Model\Entities;
use App\Entity;
    final class Category extends Entity {
        private $id;
        private $name;
        private $nbTopics;
        private $lastTopic;

        public function __construct($data){         
            $this->hydrate($data);        
        }

        /**
         * Get the value of id
         */
        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */
        public function setId($id)
        {
                $this->id = $id;
                return $this;
        }

        /**
         * Get the value of name
         */
        public function getName()
        {
                return $this->name;
        }

        /**
         * Set the value of name
         *
         * @return  self
         */
        public function setName($name)
        {
                $this->name = $name;
                return $this;
        }

        /**
         * Get the value of nbTopics
         */
        public function getNbTopics()
        {
                return $this->nbTopics;
        }

        /**
         * Set the value of nbTopics
         *
         * @return  self
         */
        public function setNbTopics($nbTopics)
        {
                $this->nbTopics = $nbTopics;
                return $this;
        }

        /**
         * Get the value of lastTopic
         */

        public function getLastTopic()

        {
                return $this->lastTopic;
        }

        /**
         * Set the value of lastTopic
         *
         * @return  self
         */

        public function setLastTopic($lastTopic)

        {
                $this->lastTopic = $lastTopic;
                return $this;
        }
        



        public function __toString()
        {
            return $this->name;
        }
    }



?>