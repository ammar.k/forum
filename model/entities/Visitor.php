<?php
namespace Model\Entities;
use App\Entity;

    final class Visitor extends Entity{
        private $id;
        private $userName;
        private $password;
        private $email;
        private $inscriptionDate;
        private $role;
        private $nbPosts;
        private $nbTopics;
        private $photoTitle;

        public function __construct($data){         
            $this->hydrate($data);        
        }

        


        /**
         * Get the value of id
         */
        public function getId()
        {
                return $this->id;
        }

        /**
         * Set the value of id
         */
        public function setId($id): self
        {
                $this->id = $id;

                return $this;
        }

        /**
         * Get the value of username
         */
        public function getUserName()
        {
                return $this->userName;
        }

        /**
         * Set the value of username
         */
        public function setUserName($userName): self
        {
                $this->userName = $userName;

                return $this;
        }

        /**
         * Get the value of password
         */
        public function getPassword()
        {
                return $this->password;
        }

        /**
         * Set the value of password
         */
        public function setPassword($password): self
        {
                $this->password = $password;

                return $this;
        }

        /**
         * Get the value of email
         */
        public function getEmail()
        {
                return $this->email;
        }

        /**
         * Set the value of email
         */
        public function setEmail($email): self
        {
                $this->email = $email;

                return $this;
        }

        /**
         * Get the value of inscriptionDate
         */
        public function getInscriptionDate()
        {
                //date today
                $date = new \DateTime();
                //check if the date is today

                $formattedDate = date('d/m/Y', strtotime($this->inscriptionDate));
                if($date->format('d/m/Y') == $formattedDate){
                    //if it is, return only the time
                    $formattedDate = date('H:i:s', strtotime($this->inscriptionDate));
                }
                else{
                    //if not, return just the date
                    $formattedDate = date('d/m/Y', strtotime($this->inscriptionDate));
                }
                return $formattedDate;
        }

        /**
         * Set the value of inscriptionDate
         */
        public function setInscriptionDate($inscriptionDate): self
        {
                $this->inscriptionDate = $inscriptionDate;

                return $this;
        }

        /**
         * Get the value of role
         */
        public function getRole()
        {
                return $this->role;
        }

        /**
         * Set the value of role
         */
        public function setRole($role): self
        {
                $this->role = $role;

                return $this;
        }

        public function hasRole($role){
            return $this->role == $role ? true : false;
        }

        /**
         * Get the value of nbPost
         */
        public function getNbPosts()
        {
                return $this->nbPosts;
        }

        /**
         * Set the value of nbPost
         */
        public function setNbPosts($nbPosts): self
        {
                $this->nbPosts = $nbPosts;

                return $this;
        }

        /**
         * Get the value of nbTopic
         */
        public function getNbTopics()
        {
                return $this->nbTopics;
        }

        /**
         * Set the value of nbTopic
         */

        public function setNbTopics($nbTopics): self
        {
                $this->nbTopics = $nbTopics;

                return $this;
        }

        /**
         * Get the value of photo
         */

        public function getPhotoTitle()
        {
                return $this->photoTitle;
        }

        /**
         * Set the value of photo
         */
        
        public function setPhotoTitle($photoTitle): self
        {
                $this->photoTitle = $photoTitle;

                return $this;
        }



        public function __toString()
        {
            return $this->userName;
        }
    }



?>