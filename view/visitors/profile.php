<?php
$topics = $result["data"]['topics'];
$user = $result["data"]["user"];
$posts = $result["data"]["posts"];

?>
        <div class="row">
            <div class="col-4">
                <h2><?= $user->getUserName() ?></h2>
                <p><?= $user->getRole() ?></p>
        
                <?php
                    if($user->getPhotoTitle() == NULL){
                        ?>
                        <img style="width:150px;height:150px;border-radius:30px;"
                        src="<?= PUBLIC_DIR ?>/img/default.jpg"></img>
                        <?php
                    }
                    else{
                        ?>
                        <img style="width:150px;height:150px;border-radius:30px;"
                        src="<?= PUBLIC_DIR ?>/img/<?=$user->getPhotoTitle()?>"></img>
                        <?php
                    }
                ?>
                
                
            <?php if(App\Session::getUser() == $user->getUserName()){?>
                <form class="photo-profil" action="index.php?ctrl=forum&action=changePhoto" 
                method="post" enctype="multipart/form-data">
                    <input type="file" name="fileToUpload" id="fileToUpload">
                    <input type="submit" name="button1" id="button1" value="Change Photo"/>

                </form>
            
            <!-- <button class="button1"><a href=""> Change Picture</a></button> -->
            <?php } ?>
   
                <p>Number Of Posts</p>
                <p><?= $user->getNbPosts() ?></p>
           
                <p>Number Of Topics</p>
                <p><?= $user->getNbTopics() ?></p>

            <p>Member since <?= $user->getInscriptionDate()?></p>
            </div>
            <div class="col-4">
        <?php
         foreach($posts as $post) { ?>
            <p>
                <a href="index.php?ctrl=forum&action=listPosts&id=<?= $post->getTopic()->getId() ?>"><?= $post->getExcerpt() ?></a>
                posted on <?= $post->getCreationDate() ?>
            </p>

        <?php } ?>
            </div>
            <div class="col-4">
            <h2>User Info</h2>
            <p style="margin-bottom:0px;padding:0px;">Username: <?= $user->getUserName() ?></p>
            <?php if(App\Session::getuser() == $user->getUserName()){?>
            <form class="mod-profil" style="margin-top:0px;"
                action="index.php?ctrl=forum&action=changeName" method="post" >
                <input type="text" name="username" placeholder="New Username">
                <input type="submit" name="button1" id="button1" value="Submit"/>
            </form>
            <p style="margin-bottom:0px;padding:0px;">Email: <?= $user->getEmail() ?></p>
            <form class="mod-profil" style="margin-top:0px;"
            action="index.php?ctrl=forum&action=changeMail" method="post">
                <input type="text" name="mail" placeholder="New Email">
                <input type="submit" name="button1" id="button1" value="Submit"/>
            </form>
            <p style="margin-bottom:0px;padding:0px;"> Change Password</p>
            <form class="mod-profil" style="margin-top:0px;"
            action="index.php?ctrl=forum&action=changePass" method="post">
                <input type="password" name="oldPass" placeholder="Old Password">
                <input type="password" name="pass1" placeholder="New Password">
                <input type="password" name="pass2" placeholder="Repeat New Password">
                <input type="submit" name="button1" id="button1" value="Submit"/>
            
            </form>
            </div>
        </div>
        <?php } ?>
