<?php
$users = $result["data"]["users"];
?>

<table class="tab-category">
    <tr>
        <th>Username</th>
        <th>Role</th>
        <th>Member since</th>
        <th>Actions</th>
    </tr>
    <?php
    foreach($users as $user) { ?>
        <tr>
            <td><a href="index.php?ctrl=forum&action=infoUser&id=<?= $user->getId() ?>"><?= $user->getUserName() ?></a></td>
            <td><?= $user->getRole() ?></td>
            <td><?= $user->getInscriptionDate() ?></td>
            <td>
                <?php 
                        if(($user->getRole() == "ROLE_ADMIN")){ 
                            if($user->getUserName() != "Ammar"){?>
                            <button style="border:2px solid orange;" class="btn-admin">
                            <a href="index.php?ctrl=security&action=removeRole&id=<?= $user->getId()?>" >Remove Admin</a></button>
                            <button style="border:2px solid red;" class="btn-admin">
                            <a href="index.php?ctrl=security&action=removeUser&id=<?= $user->getId()?>">Delete User</a></button>
                            <?php }?>
                        <?php }
                        elseif(($user->getRole() == "ROLE_MODERATOR")){ ?>
                            <button style="border:2px solid black;" class="btn-admin">
                            <a href="index.php?ctrl=security&action=removeRole&id=<?= $user->getId()?>" >Remove Moderator</a></button>
                            <button style="border:2px solid green;" class="btn-admin">
                            <a href="index.php?ctrl=security&action=makeAdmin&id=<?= $user->getId()?>" >Make Admin</a></button>
                            <button style="border:2px solid red;" class="btn-admin">
                            <a href="index.php?ctrl=security&action=removeUser&id=<?= $user->getId()?>">Delete User</a></button>
                        <?php }
                        else{ ?>
                            <button style="border:2px solid green;" class="btn-admin">
                            <a href="index.php?ctrl=security&action=makeAdmin&id=<?= $user->getId()?>" >Make Admin</a></button>
                            <button style="border:2px solid blue;" class="btn-admin">
                            <a href="index.php?ctrl=security&action=makeModerator&id=<?= $user->getId()?>">Make Moderator</a></button>
                            <button style="border:2px solid red;" class="btn-admin">
                            <a href="index.php?ctrl=security&action=removeUser&id=<?= $user->getId()?>">Delete User</a></button>
                    <?php }
                    

                ?>

            </td>
        </tr>
    <?php } ?>
</table>

<style>
    .btn-admin{
        border: 2px solid;
        color: white;
        padding: 5px 10px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 12px;
        margin: 2px 2px;
        cursor: pointer;
        border-radius: 5px;
        transition-duration: 0.4s;
        background-color: white;
        color: black;
    }
    .btn-admin:hover{
        background-color: #4CAF50;
        color: white;
    }
    .btn-admin:active{
        background-color: #5C6B77;
    }
</style>