

<div class="form-wrapper">
    <h2>Register</h2>
    <form class="reg-form" action="index.php?ctrl=security&action=register" method="post">
        <div class="form-item">
            <label for="email"></label>
            <input type="email" name="email" required="required" placeholder="Email Address"></input>
        </div>
        <div class="form-item">
            <label for="username"></label>
            <input type="text" name="username" required="required" placeholder="Username"></input>
        </div>
        <div class="form-item">
            <label for="password"></label>
            <input type="password" name="password" required="required" placeholder="Password"></input>
        </div>
        <div class="form-item">
            <label for="password2"></label>
            <input type="password" name="password2" required="required" placeholder="Confirm Password"></input>
        </div>
        <div class="button-panel">
            <input type="submit" class="button" title="Register" name="register" value="Register"></input>
        </div>
    </form>
    <div class="form-footer">
        <p><a href="index.php?ctrl=security&action=loginPage">Already have an account?</a></p>
    </div>
</div>
