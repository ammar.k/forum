<div class="form-wrapper">
    <h2>login</h2>
    <form class="reg-form" action="index.php?ctrl=security&action=login" method="post">
        
        <div class="form-item">
            <label for="mail"></label>
            <input type="email" name="email" require="required" placeholder="Email"></input>
        </div>
        <div class="form-item">
            <label for="password"></label>
            <input type="password" name="passWord" required="required" placeholder="Password"></input>
        </div>
        
        <div class="button-panel">
            <input type="submit" class="button" title="Login" name="login" value="Login"></input>
        </div>
    </form>
    <div class="form-footer">
        <p> you don't have an account? <a href="index.php?ctrl=security&action=registerPage">Register</a></p>
    
</div>