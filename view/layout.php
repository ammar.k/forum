<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.tiny.cloud/1/zg3mwraazn1b2ezih16je1tc6z7gwp5yd4pod06ae5uai8pa/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <link rel="stylesheet" href="<?= PUBLIC_DIR ?>/css/style.css">
    <title>FORUM</title>
</head>
<body>
    <div id="wrapper"> 
       
        <div id="mainpage"> 
            <header>
                <div class="logo">
                    <a href="http://localhost:8888/KOUZEHA_Ammar/forum/"><img src="<?= PUBLIC_DIR ?>/img/default.jpg" alt="logo"></a>
                </div>
                <div class="title">
                    <h1>WELCOME TO THE FORUM</h1>
                    <!-- c'est ici que les messages (erreur ou succès) s'affichent-->
                    <h3 class="message" style="color: red"><?= App\Session::getFlash("error") ?></h3>
                    <h3 class="message" style="color: green"><?= App\Session::getFlash("success") ?></h3>
                </div>
            
               
                <div class="navi" id="myNavi">
                    
                    <a href="http://localhost:8888/KOUZEHA_Ammar/forum/">Accueil</a>
                    <?php  
                        if(App\Session::getUser()){
                            ?>
                                <a href="index.php?ctrl=forum&action=listCategories">la liste des catégories</a>
                                <a href="index.php?ctrl=forum&action=infoUser&id=<?= App\Session::getUser()->getId()?>"><span class="fas fa-user"></span>&nbsp;<?= App\Session::getUser()?></a>
                                <?php
                        
                        if (App\Session::isAdmin()) {
                        ?>
                            <a href="index.php?ctrl=security&action=listUsers">Gérer les utilisateurs</a>
                        <?php
                        }?>
                                <a href="index.php?ctrl=security&action=logout">Déconnexion</a>
                            
                        <?php
                        
                        }
                        else{
                            ?>
                                <a href="index.php?ctrl=security&action=loginPage">Connexion</a>
                                <a href="index.php?ctrl=security&action=registerPage">Inscription</a>
                                <a href="index.php?ctrl=forum&action=listCategories">la liste des catégories</a>
                            
                        <?php
                        }
                    ?>
                    
                    <a href="javascript:void(0);" class="icon" onclick="myFunction1()">
                        <i class="fa fa-bars"></i>
                    </a>
                    
                    </div>
                    </header>
            
            
            <main id="forum">
                <?= $page ?>
            </main>
        </div>
        <footer>
            <p>&copy; 2020 - Forum CDA </p> <p> <a href="/home/forumRules.html">Règlement du forum</a></p>
            <p><a href="">Mentions légales</a></p>
            <!--<button id="ajaxbtn">Surprise en Ajax !</button> -> cliqué <span id="nbajax">0</span> fois-->
        </footer>
    </div>
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">
    </script>
    <script>

        $(document).ready(function(){
            $(".message").each(function(){
                if($(this).text().length > 0){
                    $(this).slideDown(500, function(){
                        $(this).delay(3000).slideUp(500)
                    })
                }
            })
            $(".delete-btn").on("click", function(){
                return confirm("Etes-vous sûr de vouloir supprimer?")
            })
            tinymce.init({
                selector: '.post',
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                ],
                toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
                content_css: '//www.tiny.cloud/css/codepen.min.css'
            });
        })

        function myFunction1() {
            var x = document.getElementById("myNavi");
            if (x.className === "navi") {
                x.className += " responsive";
            } else {
                x.className = "navi";
            }
        }

        

        /*
        $("#ajaxbtn").on("click", function(){
            $.get(
                "index.php?action=ajax",
                {
                    nb : $("#nbajax").text()
                },
                function(result){
                    $("#nbajax").html(result)
                }
            )
        })*/
    </script>
</body>
</html>