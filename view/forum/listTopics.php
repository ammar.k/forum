<?php

$topics = $result["data"]['topics'];
$category = $result["data"]['category'];

?>

<h2><a href="index.php?ctrl=forum&action=listCategories">List Categories</a> -> <?=$category->getName()?></h2>


    
<table class="tab-category">
        <tr>
            <th>Topics</th>
            <th>Creation Date</th>
            <th>Number Of Posts</th>
            <th>Last Message</th>
        </tr>
<?php
    if($topics != null){
        $topics = iterator_to_array($topics, false);
        echo "il y a ".count($topics)." topics";
    foreach($topics as $topic ){
        ?>
        <tr>
            <td><h3><a href="index.php?ctrl=forum&action=listPosts&id=<?= $topic->getId()?>"><?=$topic->getTitle()?></a></h3>
                <?php if(!$topic->getVisitor()){
                    //var_dump($topic->getVisitor());die; ?>
                    
                    <p style="font-size:14px;"> Created by Anonymous</p>
                <?php }else{?>
                    <p style="font-size:14px;"> Created by<a href="index.php?ctrl=forum&action=infoUser&id=<?= $topic->getVisitor()->getId()?>">
                    <?= $topic->getVisitor()->getUserName()?></a></p>
                <?php }?>
                
             <?php 
                if($topic->getVisitor()){
                    if((App\Session::getUser() == $topic->getVisitor()->getUserName()) || (App\Session::isAdmin())
                       || (App\Session::isModerator())){?>
                    <button class="button1"><a id="link-btn" href="index.php?ctrl=forum&action=deleteTopic&id=<?= $topic->getId()?>"> Delete</a></button>
                    <button class="button1" id="button1" onclick="showTextarea('<?=$topic->getId()?>')"><a>Modify</a></button>
                <?php
                }?>
                <form id="editForm<?=$topic->getId()?>" style="display:none;" action="index.php?ctrl=forum&action=modifyTopic&id=<?= $topic->getId()?>" method="post">
                <input type="text" name="title" placeholder="New Topic Name">
                <input type="submit" class="button1" name="button1" id="button1" value="Submit"/>
                </form>
                <?php if((App\Session::getUser() == $topic->getVisitor()->getUserName()) || (App\Session::isAdmin())
                          ||  (App\Session::isModerator())){?>
                    <button class="button1"> <a href="index.php?ctrl=forum&action=lockTopic&id=<?= $topic->getId()?>">Lock</a></button>
                    <button class="button1"> <a href="index.php?ctrl=forum&action=unlockTopic&id=<?= $topic->getId()?>">Unlock</a></button>
                <?php
                }
            }
                ?>
            </td>
            <td><?=$topic->getCreationDate()?></td>
            <td><?= $topic->getNbPosts()?></td>
            <td>
                <?php if($topic->getLastPost() == null){
                    ?>
                    No posts yet
                    <?php
                }else{
                    ?>
                    <?=$topic->getLastPost()?></br>
                    <?php

                }?>
            </td>
            <?php
                }?>
        </tr>
        
        <?php
     }
    
    else{
        echo "This category has no topics yet";
    }

    
?>
    </form>
</table>
    <?php if(App\Session::getUser() != null){?>
        <div class="topic-wrapper"> 
            <form class="topic-form" action="index.php?ctrl=forum&action=addTopic&id=<?=$category->getId()?>" method="post">
                <input type="text" id="title" name="title" placeholder="Topic Title">
                <textarea name="content" id="myTextarea" placeholder="Content"></textarea>
                <input type="submit" name="submit" class="button1" value="Add Topic">
            </form>
        </div>
    <?php
    }?>

    <script>
        //function to display a text area on click button1
        function showTextarea(id) {
                var form = document.getElementById("editForm" + id);
                if (form.style.display == "none") {
                    form.style.display = "block";
                } else {
                    form.style.display = "none";
                }
        }

                // Click event handler for the button
              /*  var button = document.getElementById("button2");
                button.onclick = function() {
                showTextarea("myTextarea");
                };*/
        

    </script>


  
