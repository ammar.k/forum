<?php
    $posts = $result["data"]['posts'];
    $topic = $result["data"]['topic'];
    $category = $result["data"]['category'];

?>


    <h2><a href="index.php?ctrl=forum&action=listCategories">List Categories </a> ->
<a href="index.php?ctrl=forum&action=listTopics&id=<?=$category->getId()?>"><?=$category->getName()?></a> ->
<?=$topic->getTitle()?>
</a>
</h2>
    
    
    <div class="topic-title">
        <h2 style="margin-top:auto;"><?= $topic->getTitle()?></h2>
        <?php if(!$topic->getVisitor()){ ?>
            <p style="font-size:12px;">Created by Anonymous</p>
        <?php }else{?>
            <p style="font-size:12px;">Created by <a href="index.php?ctrl=forum&action=infoUser&id=<?= $topic->getVisitor()->getId()?>">
            <?= $topic->getVisitor()->getUserName()?></a>
            <?php }
            ?></p>
        on <?= $topic->getCreationDate()?></p>
    </div>
    
    <table class="tab-category">
        <tr>
            <th>User</th>
            <th>Posts</th>
            
        </tr>
        <?php
        if(empty($posts)){
            echo "This topic has no posts yet";
            
            
        }
        else{
        
        foreach($posts as $post ){
            ?>
            <tr>
                <td>
                    <img style="width:60px;height:60px;border-radius:30px;" src="<?= PUBLIC_DIR ?>/img/<?=$post->getVisitor()->getPhotoTitle()?>">
                    <?php if(!$post->getVisitor()){ ?>
                                <p>Wrote by Anonymous</p> 
                    <?php }else{?>
                    <p> wrote by <a href="index.php?ctrl=forum&action=infoUser&id=<?= $post->getVisitor()->getId()?>">
                            <?=$post->getVisitor()->getUserName()?></a></p>
                    <?php }?>
                    <p style="font-size:12px;"><?=$post->getCreationDate()?></p>
                </td>
                <td>
                    <p style="font-size:18px;text-align: justify;margin-top:0px;"><?=$post->getContent()?></p>
                    <?php 
                    if($post->getVisitor()){
                        if((App\Session::getUser() == $post->getVisitor()->getUserName()) || (App\Session::isAdmin())
                            || (App\Session::isModerator())){?>
                            <button class="button1"> <a href="index.php?ctrl=forum&action=deletePost&id=<?= $post->getId()?>">Delete</a></button>
                            <button class="button1" id="button1" onclick="showTextarea('<?=$post->getId()?>')">Modify</button>
                    <?php
                }
            }
                ?>
                <form id="editForm<?=$post->getId()?>" style="display:none;" action="index.php?ctrl=forum&action=modifyPost&id=<?= $post->getId()?>" method="post">
                <textarea name="content" id="myTextarea"><?=$post->getContent()?> </textarea>
                <input type="submit" name="button1" class="button1" id="button1"
                value="Submit"/>
            </form>
                
             </td>

                
            </tr>
            
            <?php
           
    }
    }
?>
    </table>

    <?php 
    if($topic->getLocked() == 0){
        ?>

    <?php 
    
        if(App\Session::getUser() != null){?>
            <div class="topic-wrapper">
             <form class="topic-form" action="index.php?ctrl=forum&action=addPost&id=<?=$topic->getId()?>" method="post">
                <textarea name="content" cols="50" rows="10" placeholder="Post Content"></textarea>
                <input id="form-btn" class="button1" type="submit" name="submit" value="Add">
            </form>
        <?php }?>
    <?php }
    else{
        echo "This topic is locked";
    }
    
    ?>

</div>

    <script>
        //function to display a text area on click button1
        function showTextarea(id) {
                var form = document.getElementById("editForm" + id);
                if (form.style.display == "none") {
                    form.style.display = "block";
                } else {
                    form.style.display = "none";
                }
        }

                // Click event handler for the button
              /*  var button = document.getElementById("button2");
                button.onclick = function() {
                showTextarea("myTextarea");
                };*/
        

    </script>

